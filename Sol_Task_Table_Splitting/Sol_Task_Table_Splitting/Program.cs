﻿using Sol_Task_Table_Splitting.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sol_EF_Table_Spliting
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<UserEntity> listUserEntityObj =
                    await new UserDal().GetUser();

            }).Wait();
        }
    }
}
