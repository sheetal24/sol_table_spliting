﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Task_Table_Splitting.Entity
{
    public class UserEntity
    {
        public int? UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public UserLoginEntity UserLogin { get; set; }

        public UserCommunicationEntity UserCommunication { get; set; }
    }
}
