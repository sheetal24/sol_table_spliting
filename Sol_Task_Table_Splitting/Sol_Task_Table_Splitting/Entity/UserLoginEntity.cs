﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Task_Table_Splitting.Entity
{
   public class UserLoginEntity
    {
        public int? UserId { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}
