﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Task_Table_Splitting.Entity
{
    public class UserCommunicationEntity
    {
        public int? UserId { get; set; }

        public String MobileNo { get; set; }

        public String EmailId { get; set; }
    }
}
